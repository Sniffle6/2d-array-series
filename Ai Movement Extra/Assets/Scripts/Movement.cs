﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Entity player;
    private void Start()
    {
        player = GetComponent<Entity>();
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && player.TurnActive)
        {
            var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2Int result = Utils.WorldToCellPosition(pos.x, pos.y);
            player.Move(GridManager.instance.Grid, result.x, result.y);
        }
    }
}
