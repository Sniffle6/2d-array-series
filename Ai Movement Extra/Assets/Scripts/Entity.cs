﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Entity : MonoBehaviour
{
    public int walkDistance;
    public int health;
    public int MaxHealth;
    public int damage;
    public int defence;
    public int X;
    public int Y;
    public List<Spot> path = new List<Spot>();
    IEnumerator corutine;
    public bool TurnActive = false;
    public abstract void OnTurnStart();
    public void Move(Cell[,] grid, int x, int y)
    {
        if (path != null && path.Count > 0)
        {
            for (int i = 0; i < path.Count; i++)
                grid[path[i].X, path[i].Y].Renderer.color = Color.white;
            path.Clear();
        }
        if (corutine != null)
            StopCoroutine(corutine);
        path = GridManager.instance.astar.CreatePath(grid, new Vector2Int(X, Y), new Vector2Int(x, y), walkDistance);

        if (path == null || path.Count <= 0)
            return;
        corutine = Moving(grid, path);
        StartCoroutine(corutine);
    }
    IEnumerator Moving(Cell[,] grid, List<Spot> path)
    {
        var pathIndex = path.Count - 1;
        while (pathIndex >= 0 && path != null)
        {
            Vector3 goal = grid[path[pathIndex].X, path[pathIndex].Y].gameObject.transform.position;


            transform.Translate((goal - transform.position) * Time.fixedDeltaTime * 15);
            if (Vector3.Distance(transform.position, goal) < 0.025f)
            {
                grid[path[pathIndex].X, path[pathIndex].Y].Renderer.color = Color.white;
                grid[path[pathIndex].X, path[pathIndex].Y].entity = null;

                path.RemoveAt(pathIndex);

                transform.position = goal;
                pathIndex--;
                if (pathIndex >= 0)
                {
                    X = path[pathIndex].X;
                    Y = path[pathIndex].Y;
                    GetComponent<SpriteRenderer>().sortingOrder = Utils.Rows-Y+1;
                    grid[X, Y].entity = this;
                    //if (pathIndex != 0)
                    //{
                    //    var newX = path[0].X;
                    //    var newY = path[0].Y;
                    //    path = GridManager.instance.astar.CreatePath(grid, new Vector2Int(X, Y), new Vector2Int(newX, newY), walkDistance);
                    //}
                }
            }
            yield return new WaitForFixedUpdate();
        }
        grid[X, Y].entity = this;
        PlaceOnGrid(grid, X, Y);
        TurnActive = false;
        GameManager.instance.OnTurnFinished();
    }
    public abstract void TakeDamage();
    public void PlaceOnGrid(Cell[,] grid, int x, int y)
    {
        X = x;
        Y = y;
        GetComponent<SpriteRenderer>().sortingOrder = Utils.Rows - y+1;
        grid[x, y].entity = this;
        transform.position = grid[x, y].gameObject.transform.position;
    }
}
