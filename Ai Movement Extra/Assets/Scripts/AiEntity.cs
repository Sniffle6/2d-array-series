﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiEntity : Entity
{
    AiMovement movement;
    private void Start()
    {
        movement = GetComponent<AiMovement>();
    }
    public override void OnTurnStart()
    {
        TurnActive = true;
        movement.Move();
    }

    public override void TakeDamage()
    {
        throw new System.NotImplementedException();
    }
}
