﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Instance
    public static GameManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }
    #endregion
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public Entity player;
    public List<Entity> entities = new List<Entity>();
    public GridManager g_Manager;
    public int Turn = 0;
    // Start is called before the first frame update
    void Start()
    {
        g_Manager = GridManager.instance;
        player = Instantiate(playerPrefab).GetComponent<Entity>();
        player.PlaceOnGrid(g_Manager.Grid, 1, 1);
        entities.Add(player);
        for (int i = 0; i < 5; i++)
        {
            entities.Add(Instantiate(enemyPrefab).GetComponent<Entity>());
            entities[entities.Count - 1].PlaceOnGrid(g_Manager.Grid, Random.Range(1, 10), Random.Range(1, 5));
        }
        OnTurnStart();
    }
    public void OnTurnStart()
    {
        entities[Turn].OnTurnStart();
    }
    public void OnTurnFinished()
    {
        Turn += 1;
        if (Turn > entities.Count - 1)
            Turn = 0;
        OnTurnStart();
    }
}
