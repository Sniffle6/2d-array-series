﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Cell
{
    readonly GameObject Prefab;
    public GameObject gameObject;
    public Transform transform;
    public SpriteRenderer Renderer;
    public CellDecoration Decoration;
    public Entity entity;
    public TileThemeObject Theme;
    public int X;
    public int Y;
    public Cell(GameObject prefab, TileThemeObject theme, int x, int y)
    {
        X = x;
        Y = y;
        Prefab = prefab;
        gameObject = GameObject.Instantiate(prefab, Utils.CellToWorldPosition(x, y), Quaternion.identity);
        transform = gameObject.transform;
        Renderer = gameObject.GetComponent<SpriteRenderer>();
       // gameObject.name = string.Concat("X: " , x , "Y: " , y); //"X: " + x + "Y: " + y;
        Theme = theme;
        Renderer.sprite = Theme.tiles[Utils.GetTile(x, y)];
        entity = null;
        Decoration = new CellDecoration();
        if (!Utils.IsEdgeTile(x, y))
        {
            if (Random.Range(0.0f, 1.0f) > 1.75f)
                AddDecoration();
        }
    }
    public void UpdateTileTheme(TileThemeObject theme, float fillRate = 0.5f)
    {
        Theme = theme;
        Renderer.sprite = theme.tiles[Utils.GetTile(X, Y)];
        ClearDecoration();
        if (!Utils.IsEdgeTile(X, Y) && Random.Range(0.0f, 1.0f) > fillRate)
            AddDecoration();
        // Decoration.UpdateTile(theme);
    }
    public void ClearDecoration()
    {
        GameObject.Destroy(Decoration.gameObject);
        Decoration = new CellDecoration();
    }
    public void AddDecoration()
    {
        if (!Decoration.isCreated)
            Decoration = new CellDecoration(Prefab, Theme.decorations[Random.Range(0, Theme.decorations.Length)], X, Y, transform);
    }
}

public struct CellDecoration
{
    public bool isCreated;
    public GameObject gameObject;
    public SpriteRenderer renderer;
    public TileDecorationObject decoration;
    public CellDecoration(GameObject prefab, TileDecorationObject decor, int x, int y, Transform parent = null)
    {
        gameObject = GameObject.Instantiate(prefab, Utils.CellToWorldPosition(x, y), Quaternion.identity, parent);
        //gameObject.name = string.Concat("Child of - X: " , x , "Y: " , y); //"Child of - X: " + x + "Y: " + y;
        renderer = gameObject.GetComponent<SpriteRenderer>();
        decoration = decor;
        renderer.sortingOrder = Utils.Rows - y;
        renderer.sprite = decoration.sprite;
        isCreated = true;
    }
    public void UpdateTile(TileThemeObject theme)
    {
        if (renderer)
            renderer.sprite = theme.decorations[Random.Range(0, theme.decorations.Length)].sprite;
    }
}
