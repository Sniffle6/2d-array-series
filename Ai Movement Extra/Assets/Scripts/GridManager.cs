﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    #region Instance
    public static GridManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }
    #endregion

    public GameObject tilePrefab;
    public TileThemeObject[] Themes;
    public int currentTheme;
    public Cell[,] Grid;
    public Astar astar;
    // Start is called before the first frame update
    void Start()
    {
        SetUpGrid();
        astar = new Astar();
        
    }
    public void SetUpGrid()
    {
        Utils.Vertical = (int)Camera.main.orthographicSize;
        Utils.Horizontal = Utils.Vertical * Screen.width / Screen.height;
        Utils.Columns = Utils.Horizontal * 2;
        Utils.Rows = Utils.Vertical * 2;
        Grid = new Cell[Utils.Columns, Utils.Rows];
        Grid.Fill(tilePrefab, Themes[currentTheme]);
    }
    public void UpdateTileTheme(float fillrate = 0.5f)
    {
        Grid.Color(Color.white);
        currentTheme = currentTheme < Themes.Length - 1 ? currentTheme += 1 : 0;
        Grid.Update(Themes[currentTheme], fillrate);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            UpdateTileTheme();
        if (Input.GetKeyDown(KeyCode.Q))
            UpdateTileTheme(0.9f);
        if (Input.GetKeyDown(KeyCode.W))
            UpdateTileTheme(0.1f);


    }

}

public static class ExtensionMethods
{
    public static void Fill(this Cell[,] grid, GameObject prefab, TileThemeObject theme)
    {
        for (int i = 0; i < Utils.Columns; i++)
            for (int j = 0; j < Utils.Rows; j++)
                grid[i, j] = new Cell(prefab, theme, i, j);
    }
    public static void Update(this Cell[,] grid, TileThemeObject theme, float fillrate = 0.5f)
    {
        for (int i = 0; i < Utils.Columns; i++)
            for (int j = 0; j < Utils.Rows; j++)
                grid[i, j].UpdateTileTheme(theme, fillrate);
    }
    public static void Color(this Cell[,] grid, Color color)
    {
        for (int i = 0; i < Utils.Columns; i++)
            for (int j = 0; j < Utils.Rows; j++)
                grid[i, j].Renderer.color = color;
    }
}