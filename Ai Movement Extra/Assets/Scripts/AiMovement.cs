﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiMovement : MonoBehaviour
{
    Entity entity;
    // Start is called before the first frame update
    void OnEnable()
    {
        entity = GetComponent<Entity>();
    }
    public void Move()
    {
        // int attemps = 0;
        int x = GameManager.instance.player.X;
        int y = GameManager.instance.player.Y;

        while ((GridManager.instance.Grid[x, y].Decoration.isCreated && GridManager.instance.Grid[x, y].Decoration.decoration.height == 1) || GridManager.instance.Grid[x, y].entity)
        {
            int index = 1;
            Spot spot = new Spot(x, y, 0, 0, 0, 0);
            if (x < Utils.Columns - index)
            {
                spot.Neighboors.Add(new Spot(x + index, y, 0, 0, 0, 0));
            }
            if (x >= 0 + index)
                spot.Neighboors.Add(new Spot(x - index, y, 0, 0, 0, 0));

            if (y < Utils.Rows - index)
                spot.Neighboors.Add(new Spot(x, y + index, 0, 0, 0, 0));

            if (y >= 0 + index)
                spot.Neighboors.Add(new Spot(x, y - index, 0, 0, 0, 0));

            if (x >= 0 + index && y >= 0 + index)
                spot.Neighboors.Add(new Spot(x - index, y - index, 0, 0, 0, 0));

            if (x < Utils.Columns - 1 && y >= 0 + index)
                spot.Neighboors.Add(new Spot(x + index, y - index, 0, 0, 0, 0));

            if (x > 0 && y < Utils.Rows - index)
                spot.Neighboors.Add(new Spot(x - index, y + index, 0, 0, 0, 0));

            if (x < Utils.Columns - index && y < Utils.Rows - index)
                spot.Neighboors.Add(new Spot(x + index, y + index, 0, 0, 0, 0));

            for (int i = 0; i < spot.Neighboors.Count; i++)
            {
                var s = spot.Neighboors[i];
                if ((!GridManager.instance.Grid[s.X, s.Y].Decoration.isCreated && !GridManager.instance.Grid[s.X, s.Y].entity) || (GridManager.instance.Grid[s.X, s.Y].Decoration.isCreated && GridManager.instance.Grid[s.X, s.Y].Decoration.decoration.height < 1 && !GridManager.instance.Grid[s.X, s.Y].entity))
                {
                    x = s.X;
                    y = s.Y;
                    break;
                }
                else
                {

                    x = Random.Range(0, Utils.Columns);
                    y = Random.Range(0, Utils.Rows);
                }
            }
        }
        entity.Move(GridManager.instance.Grid, x, y);
    }
}
