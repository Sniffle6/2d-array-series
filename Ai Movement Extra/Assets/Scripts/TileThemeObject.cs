﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="New Tile Theme1", menuName = "Tiles/Theme1")]
public class TileThemeObject : ScriptableObject
{
    public Sprite[] tiles;
    public TileDecorationObject[] decorations;
}
