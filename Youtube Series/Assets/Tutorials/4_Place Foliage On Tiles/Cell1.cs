﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Cell1
{
    readonly GameObject Prefab;
    public GameObject gameObject;
    public SpriteRenderer Renderer;
    public CellDecoration Decoration;
    public TileThemeObject1 Theme;
    public int X;
    public int Y;
    public Cell1(GameObject prefab, TileThemeObject1 theme, int x, int y)
    {
        X = x;
        Y = y;
        Prefab = prefab;
        gameObject = GameObject.Instantiate(prefab, Utils1.GetGridPosition(x, y), Quaternion.identity);
        Renderer = gameObject.GetComponent<SpriteRenderer>();
        gameObject.name = "X: " + x + "Y: " + y;
        Theme = theme;
        Renderer.sprite = Theme.tiles[Utils1.GetTile(x, y)];

        Decoration = new CellDecoration();
        if (!Utils1.IsEdgeTile(x, y))
        {
            if (Random.Range(0.0f, 1.0f) > 0.5f)
                AddDecoration();
        }
    }
    public void UpdateTileTheme(TileThemeObject1 theme, float fillRate = 0.5f)
    {
        Theme = theme;
        Renderer.sprite = theme.tiles[Utils1.GetTile(X, Y)];
        ClearDecoration();
        if (!Utils1.IsEdgeTile(X, Y) && Random.Range(0.0f, 1.0f) > fillRate)
            AddDecoration();
        // Decoration.UpdateTile(theme);
    }
    public void ClearDecoration()
    {
        GameObject.Destroy(Decoration.gameObject);
        Decoration = new CellDecoration();
    }
    public void AddDecoration()
    {
        if (!Decoration.isCreated)
            Decoration = new CellDecoration(Prefab, Theme.decorations[Random.Range(0, Theme.decorations.Length)], X, Y, gameObject.transform);
    }
}

public struct CellDecoration
{
    public bool isCreated;
    public GameObject gameObject;
    public SpriteRenderer renderer;
    public TileDecorationObject decoration;
    public CellDecoration(GameObject prefab, TileDecorationObject decor, int x, int y, Transform parent = null)
    {
        gameObject = GameObject.Instantiate(prefab, Utils1.GetGridPosition(x, y), Quaternion.identity, parent);
        gameObject.name = "Child of - X: " + x + "Y: " + y;
        renderer = gameObject.GetComponent<SpriteRenderer>();
        decoration = decor;
        renderer.sortingOrder = 1;
        renderer.sprite = decoration.sprite;
        isCreated = true;
    }
    public void UpdateTile(TileThemeObject1 theme)
    {
        if (renderer)
            renderer.sprite = theme.decorations[Random.Range(0, theme.decorations.Length)].sprite;
    }
}
