﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManagerFoliage : MonoBehaviour
{
    public GameObject tilePrefab;
    public TileThemeObject1[] Themes;
    public int currentTheme;
    public Cell1[,] Grid;
    // Start is called before the first frame update
    void Start()
    {
        SetUpGrid();
    }
    public void SetUpGrid()
    {
        Utils1.Vertical = (int)Camera.main.orthographicSize;
        Utils1.Horizontal = Utils1.Vertical * Screen.width / Screen.height;
        Utils1.Columns = Utils1.Horizontal * 2;
        Utils1.Rows = Utils1.Vertical * 2;
        Grid = new Cell1[Utils1.Columns, Utils1.Rows];

        //for (int i = 0; i < Utils1.Columns; i++)
        //    for (int j = 0; j < Utils1.Rows; j++)
        //        Grid[i, j] = new Cell1(tilePrefab, Themes[currentTheme], i, j);
        Grid.Fill(tilePrefab, Themes[currentTheme]);
    }
    public void UpdateTileTheme(float fillrate = 0.5f)
    {
        currentTheme = currentTheme < Themes.Length - 1 ? currentTheme += 1 : 0;
        Grid.Update(Themes[currentTheme], fillrate);
        //for (int i = 0; i < Utils1.Columns; i++)
        //    for (int j = 0; j < Utils1.Rows; j++)
        //        Grid[i, j].UpdateTileTheme(Themes[currentTheme], fillrate);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            UpdateTileTheme();
        if (Input.GetKeyDown(KeyCode.Q))
            UpdateTileTheme(0.9f);
        if (Input.GetKeyDown(KeyCode.W))
            UpdateTileTheme(0.1f);
    }

}

public static class ExtensionMethods
{
    public static void Fill(this Cell1[,] grid, GameObject prefab, TileThemeObject1 theme)
    {
        for (int i = 0; i < Utils1.Columns; i++)
            for (int j = 0; j < Utils1.Rows; j++)
                grid[i, j] = new Cell1(prefab, theme, i, j);
    }
    public static void Update(this Cell1[,] grid, TileThemeObject1 theme, float fillrate = 0.5f)
    {
        for (int i = 0; i < Utils1.Columns; i++)
            for (int j = 0; j < Utils1.Rows; j++)
                grid[i, j].UpdateTileTheme(theme, fillrate);
    }
}