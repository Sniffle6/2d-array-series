﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManagerTileThemes : MonoBehaviour
{
    public GameObject tilePrefab;
    public TileThemeObject[] Themes; 
    public int currentTheme;
    public Cell[,] Grid;
    // Start is called before the first frame update
    void Start()
    {
        SetUpGrid();
    }
    public void SetUpGrid()
    {
        Utils.Vertical = (int)Camera.main.orthographicSize;
        Utils.Horizontal = Utils.Vertical * Screen.width / Screen.height;
        Utils.Columns = Utils.Horizontal * 2;
        Utils.Rows = Utils.Vertical * 2;
        Grid = new Cell[Utils.Columns, Utils.Rows];
        for (int i = 0; i < Utils.Columns; i++)
            for (int j = 0; j < Utils.Rows; j++)
                Grid[i, j] = new Cell(tilePrefab, Themes[currentTheme].tiles[Utils.GetTile(i, j)], i, j);
    }
    public void UpdateTileTheme(int index)
    {
        currentTheme = index;
        for (int i = 0; i < Utils.Columns; i++)
            for (int j = 0; j < Utils.Rows; j++)
                Grid[i, j].UpdateTile(Themes[currentTheme].tiles[Utils.GetTile(i, j)]);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            UpdateTileTheme(currentTheme < Themes.Length - 1 ? currentTheme += 1 : 0);
    }

}
